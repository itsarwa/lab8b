import java.util.Random;

public class Board {
    private Tile[][] grid;

    public Board() {
        // Initializing grid 5x5
		final int size =5;
        grid = new Tile[size][size];

        // Initializing each tile inside grid as blank and a random ones as hidden_wall
		Random rng= new Random();
        for (int i = 0; i < grid.length; i++) {
			
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] = Tile.BLANK;
						
            }
			int n = rng.nextInt(grid[i].length);
				grid[i][n] = Tile.HIDDEN_WALL;
        }
		
    }

    public String toString() {
        String result = "";
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                result += grid[i][j].getName() + " ";
            }
            result += "\n";
        }
        return result;
    }

    public int placeToken(int row, int col) {
        // Checking if row and column values are in the correct range
        if (row < 0 || row >= grid.length || col < 0 || col >= grid[0].length) {
            return -2; // Invalid position
        }

        //checking if there is a castle or wall
        else if (grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL) {
            //grid[row][col] = playerToken; // Place the player's token
            return -1;
		}
		// Checking if there is a hidden wall and set it to wall
		else if (grid[row][col] == Tile.HIDDEN_WALL){
			grid[row][col] = Tile.WALL;
			return 1;
		}
		else {
			grid[row][col] = Tile.CASTLE;
			return 0;
        //} else {
          //  return false; // Position is already occupied
        }
    }
}
