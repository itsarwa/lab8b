import java.util.Scanner;

public class BoardGameApp{
	public static void main(String[] args){
		//testing if variables work
	
	//System.out.println(Tile.BLANK);
	//System.out.println(Tile.BLANK.getName());
	
	//System.out.println(Tile.WALL);
	//System.out.println(Tile.WALL.getName());
	
	//System.out.println(Tile.CASTLE);
	//System.out.println(Tile.CASTLE.getName());
	
	//System.out.println(Tile.HIDDEN_WALL);
	//System.out.println(Tile.HIDDEN_WALL.getName());
	
	Scanner scanner = new Scanner(System.in);
	System.out.println("Welcome to the Board Game!");
	int numCastles = 5;
	int turns = 0;
	Board board= new Board();
	
	while(numCastles>0 && turns< 8){
		
	System.out.println(board);
	System.out.println(numCastles);
	System.out.println(turns);
	
	System.out.print("Player, enter row (0-5): ");
            int row = scanner.nextInt();
            System.out.print("Player, enter column (0-5): ");
            int col = scanner.nextInt();
			
			int response=board.placeToken(row,col);
			while( response == -1 || response == -2){
				System.out.println( "Invalid number! Please re-enter a row: ");
				int row2 = scanner.nextInt();
				System.out.println( "Please re-enter a column: ");
				int col2 = scanner.nextInt();
			}
			if( response == 1){
				System.out.println("There is a wall! You lose a turn!");
				turns++;
			}else{
				System.out.println(" A Castle was successfully placed!");
				turns++;
				numCastles--;
			}
			
	}
	if(numCastles == 0){
		System.out.println("You won!");
	}else{
		System.out.println("You lost..");
		}
		System.out.print(board);
	}
}